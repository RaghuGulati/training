import 'dart:io';

class TestClass{
	int? n;
	int? func(){
		return n;
	}
}


void function(){
	print("This is function");	
}

void main(){

	int n1 = 25, n2 = 26;
	//print(n1,n2);
	print("$n1 + $n2 = ${n1 + n2}");
	print("$n1 - $n2 = ${n1 - n2}");
	print("$n1 * $n2 = ${n1 * n2}");
	print("$n1 / $n2 = ${n1 / n2}");

	//Number(int, double), Boolean

	bool? num;
	
	//dynamic s = int.parse(stdin.readLineSync()!);
	// ?, ! 

	// String ------- String?

	//print(s is int);

	/*if(s is int){
		print("Yeah!!! You entered integer");
	}

	else{
		print("Oops!!!! Please enter integer");
	}*/	
	

	print("Welcome to dart");

	function();

	TestClass c = new TestClass();
	TestClass c2 = new TestClass();

	c.n = 25;
	//c.func();

	c2.n = 36;
	//c2.func();

	print("${c2.func()} + ${c.func()}");
	print("${c2.n! + c.n!}");

}



