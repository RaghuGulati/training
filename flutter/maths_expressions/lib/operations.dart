import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class operations extends StatefulWidget {
  const operations({Key? key}) : super(key: key);

  @override
  State<operations> createState() => _operationsState();
}

class _operationsState extends State<operations> {
  int? no1, no2, sum;
  final myController1 = TextEditingController();
  final myController2 = TextEditingController();

  @override
  void dispose() {
    // TODO: implement dispose
    myController1.dispose();
    myController1.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Mathematical Operations"),
      ),

      body: Container(
        child: Column(
          children: <Widget>[
            TextField(
                decoration: InputDecoration(labelText: "Enter your number1"),
                keyboardType: TextInputType.number,
                controller: myController1,
            ),

            TextField(
                decoration: InputDecoration(labelText: "Enter your number2"),
                keyboardType: TextInputType.number,
                controller: myController2,
            ),
            SizedBox(height: 50,),
            ElevatedButton(
              onPressed: (() {
                setState(() {
                  no1 = int.tryParse(myController1.text);
                  no2 = int.tryParse(myController2.text);
                  sum = no1! + no2!;
                });
              }), 
              child: Text("Add")),
            SizedBox(height: 50,),
            Container(
              padding: EdgeInsets.all(25),
              color: Colors.blue,
              width: 150,
              height: 80,
              child: Text("Answer = ${sum}", style: TextStyle(color: Colors.white),),
            ),
            ]
          ),
      ),
    );
  }
}