import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home:MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void increment() {
    setState(() {
      _counter ++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text("My Resume")),
      ),
      body: Container(
        color: Colors.red[900],
        child: Padding(
          padding: EdgeInsets.fromLTRB(15.0,0,15.0,0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(child: const SizedBox(height: 25,)),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Image(
                    image: AssetImage('assets/myimg.jpg',)
                    ),
                  SizedBox(width: 50,),
                  Text("${_counter}", style: TextStyle(color: Colors.white, fontSize: 40),),
                ],
              ),
              const SizedBox(height: 25,),
              Center(
                child: const Text(
                  'Raghu Gulati',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                  ),
              ),
      
              Center(
                child: Text(
                  'Phno: xxxxxxxxxx',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
              ),
      
      
              Center(
                child: Text(
                  'Email: xxxxx@xxxx.com',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
              ),
              const SizedBox(height: 25,),
              Center(
                child: SizedBox(
                  child: Center(
                    child: const Divider(
                      color: Colors.white,
                      thickness: 1,
                      ),
                  ),
                ),
              ),
      
              
               
      /*             Padding(
                  padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                  child: const Divider(
                    color: Colors.white,
                    thickness: 1,
                    ),
                ),*/
               

               Container(
                height: 450,
                padding: EdgeInsets.all(10),
                color: Colors.red[700],
                child: new ListView(
                    children: [
                              SizedBox(height: 25),
                              Text(
                                "Schooling",
                                style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.white,
                                ),
                                ),
                
                                Padding(
                                  padding: EdgeInsets.fromLTRB(15.0,0,0,6),
                                  child: Text(
                                  "Class 10th % or SGPA (CBSE) \nClass 12th % (CBSE)",
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.white,
                                  ),
                                  ),
                                ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                                child: const Divider(
                                  color: Colors.white,
                                  thickness: 1,
                                  ),
                              ),

                              //////////////////////////////////////////////////////////////////
                              SizedBox(height: 25),
                              Text(
                                "Academics",
                                style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.white,
                                ),
                                ),
                
                                Padding(
                                  padding: EdgeInsets.fromLTRB(15.0,0,0,6),
                                  child: Text(
                                  "I am a Computer Science Engineer who completed the Btech Degree in 2021 from GNDEC Ludhiana\nSGPA secured: s",
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.white,
                                  ),
                                  ),
                                ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                                child: const Divider(
                                  color: Colors.white,
                                  thickness: 1,
                                  ),
                              ),

                              ///////////////////////////////////////////////////////////
                              SizedBox(height: 25),
                              Text(
                                "Skill Set",
                                style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.white,
                                ),
                                ),
                
                                Padding(
                                  padding: EdgeInsets.fromLTRB(15.0,0,0,6),
                                  child: Text(
                                  "My Skills:------------------",
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.white,
                                  ),
                                  ),
                                ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                                child: const Divider(
                                  color: Colors.white,
                                  thickness: 1,
                                  ),
                              ),

                              //////////////////////////////////////////////////////////////
                              SizedBox(height: 25),
                              Text(
                                "Field of Interest(s)",
                                style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.white,
                                ),
                                ),
                
                                Padding(
                                  padding: EdgeInsets.fromLTRB(15.0,0,0,6),
                                  child: Text(
                                  "My interest in cs(or any field)",
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.white,
                                  ),
                                  ),
                                ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                                child: const Divider(
                                  color: Colors.white,
                                  thickness: 1,
                                  ),
                              ),

                              //////////////////////////////////////////////////////////////////////
                              SizedBox(height: 25),
                              Text(
                                "Hobbies",
                                style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.white,
                                ),
                                ),
                
                                Padding(
                                  padding: EdgeInsets.fromLTRB(15.0,0,0,6),
                                  child: Text(
                                  "My hobbies and interests",
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.white,
                                  ),
                                  ),
                                ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                                child: const Divider(
                                  color: Colors.white,
                                  thickness: 1,
                                  ),
                              ),

                              //////////////////////////////////////////////
                              SizedBox(height: 25),
                              Text(
                                "Anyother Field",
                                style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.white,
                                ),
                                ),
                
                                Padding(
                                  padding: EdgeInsets.fromLTRB(15.0,0,0,6),
                                  child: Text(
                                  "Anyother field description",
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.white,
                                  ),
                                  ),
                                ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                                child: const Divider(
                                  color: Colors.white,
                                  thickness: 1,
                                  ),
                              ),


                              
                              //////////////////////////////////////////////
                              SizedBox(height: 25),
                              Text(
                                "Anyother Field",
                                style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.white,
                                ),
                                ),
                
                                Padding(
                                  padding: EdgeInsets.fromLTRB(15.0,0,0,6),
                                  child: Text(
                                  "Anyother field description",
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.white,
                                  ),
                                  ),
                                ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                                child: const Divider(
                                  color: Colors.white,
                                  thickness: 1,
                                  ),
                              ),
                
                    ],
                ),
              ),
                          
                          
                    ],
                  ),
               ),
      ),
        floatingActionButton: InkWell(
          splashColor: Colors.blue,
          onLongPress: () {
            setState(() {
                _counter += 2;
              });
          },
          child: FloatingActionButton( //Floating action button on Scaffold
            onPressed: (){ 
              increment();
            },
        
            child: Icon(Icons.add), //icon inside button
          ),
        ),

  floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
  //floating action button position to center

  bottomNavigationBar: Container(
    color: Colors.red,
    child: BottomAppBar( //bottom navigation bar on scaffold
      color:Colors.red,
      shape: CircularNotchedRectangle(), //shape of notch
      notchMargin: 5, //notche margin between floating button and bottom appbar
      child: Row( //children inside bottom appbar
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("This is bottom navbar.... click the button to see the fun", 
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
            )
        ],
      ),
    ),
  ),
        );}}
                  

                  
                  
      
      
          
